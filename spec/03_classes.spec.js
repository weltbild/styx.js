describe("styx.js Class Generation Tests", function () {



    it("throws an error if no classname is defined", function () {
        expect(
            function(){
                sx.build({});
            }
        ).toThrow(
            new Error('StyxJsClassDefException: please define a name for the class')
        );
    });

    it("creates a class in the standard namespace", function () {

        sx.build({
            name: 'MyClass',
            m: {
                testMethod: function () {
                    return 1;
                }
            }
        });

        var myClass = new sx.MyClass();

        expect(myClass.testMethod()).toBe(1);
    });


    it("creates a class in a custom namespace", function () {

        sx.build({
            name: 'MyClass2',
            nsp: 'com.test',
            m: {
                testMethod: function () {
                    return 1;
                }
            }
        });

        var myClass = new com.test.MyClass2();

        expect(myClass.testMethod()).toBe(1);
    });




    var trait = {
        myVar: 1,

        testFunc: function () {
            this.myVar += 1;
            return this.myVar;
        }
    };

    it("can call a trait in a class", function () {
        sx.build({
            name: 'MyClass3',
            nsp: 'com.test',
            traits: [trait]
        });

        var myClass = new com.test.MyClass3();

        expect(myClass.testFunc()).toBe(2);
    });


    it("can call a trait in two different classes and returns the same value", function () {
        sx.build({
            name: 'MyClass4',
            nsp: 'com.test',
            traits: [trait]
        });

        sx.build({
            name: 'MyClass5',
            nsp: 'com.test',
            traits: [trait]
        });
        var myClassOne = new com.test.MyClass4();
        var myClassTwo = new com.test.MyClass5();

        expect(myClassOne.testFunc()).toBe(2);
        expect(myClassTwo.testFunc()).toBe(2);
    });


    it("can call a parent constructor", function () {
        sx.build({
            name: 'MyClass6',
            nsp: 'com.test',
            m: {
                construct: function () {
                    this.testVar = 1;
                },
                testVar: 0
            }
        });
        sx.build({
            name: 'MyClass7',
            nsp: 'com.test',
            parent: com.test.MyClass6,
            m: {
                returnTestVar: function () {
                    return this.testVar;
                }
            }
        });

        var myClass = new com.test.MyClass7();

        expect(myClass.returnTestVar()).toBe(1);
    });

    it("can call a parent constructor with multiple arguments", function () {
        sx.build({
            name: 'MyClass8',
            nsp: 'com.test',
            m: {
                construct: function (arg1, arg2, arg3) {
                    this.testVar = arg3;
                },
                testVar: 0
            }
        });
        sx.build({
            name: 'MyClass9',
            nsp: 'com.test',
            parent: com.test.MyClass8,
            m: {
                returnTestVar: function () {
                    return this.testVar;
                }
            }
        });

        var myClass = new com.test.MyClass9('one', 'two', 1);

        expect(myClass.returnTestVar()).toBe(1);
    });

    it("can call a parent constructor and his own", function () {
        sx.build({
            name: 'MyClass10',
            nsp: 'com.test',
            m: {
                construct: function () {
                    this.testVar = 1;
                },
                testVar: 0
            }
        });
        sx.build({
            name: 'MyClass11',
            nsp: 'com.test',
            parent: com.test.MyClass10,
            m: {
                construct: function () {
                    this.superclass.construct.apply(this);
                    this.secondTestVar = 2;
                },
                returnTestVar: function () {
                    return this.testVar;
                }
            }
        });

        var myClass = new com.test.MyClass11();

        expect(myClass.returnTestVar()).toBe(1);
        expect(myClass.secondTestVar).toBe(2);
    });

    it("calls the contructor method 'construct' at creation time", function () {
        sx.build({
            name: 'MyClass12',
            nsp: 'com.test',
            m: {
                construct: function () {
                    this.testVar = 3;
                },
                testVar: 0
            }
        });

        var myClass = new com.test.MyClass12();
        expect(myClass.testVar).toBe(3);
    });
});
