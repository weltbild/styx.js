module.exports = function (config) {
    config.set({
        frameworks: ['jasmine'],
        basePath: '.',
        files: [
            {pattern: 'spec/fixtures/styx-js*.html', watched: true, included: false, served: true},
            'lib/jquery-1.9.js',
            'spec/lib/jasmine-jquery-1.5.2.js',
            'src/styx.js',
            'spec/*.spec.js'
        ],
        exclude: [],
        reporters: ['progress', 'junit'],
        junitReporter: {
            outputFile: 'log/test-results.xml'
        },
        port: 8008,
        runnerPort: 9100,
        colors: true,
        autoWatch: true,
        browsers: ['PhantomJS'],
        captureTimeout: 5000,
        singleRun: false,
        reportSlowerThan: 500
    });
};
